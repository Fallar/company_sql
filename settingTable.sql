﻿Create table Workers(Worker_Id serial primary key, Name char(30) not null, Surname char(30) not null);

Insert Into workers (Name,Surname) Values('Jan','Abacki');
Insert Into workers (Name,Surname) Values('Jan','Babacki');
Insert Into workers (Name,Surname) Values('Jan','Cabacki');
Insert Into workers (Name,Surname) Values('Jan','Dabacki');
Insert Into workers (Name,Surname) Values('Jan','Ebacki');
Insert Into workers (Name,Surname) Values('Jan','Fabacki');
Insert Into workers (Name,Surname) Values('Jan','Gabacki');
Insert Into workers (Name,Surname) Values('Jan','Habacki');
Insert Into workers (Name,Surname) Values('Jan','Kabacki');
Insert Into workers (Name,Surname) Values('Jan','Labacki');

Create table Structure(Id SERIAL primary key, Worker_Id int not null, SupVis_Id int);

Insert into structure (Worker_Id) select Worker_Id from Workers;

Update structure 
Set SupVis_Id = 1
Where Worker_Id between 2 and 4;

Update structure 
Set SupVis_Id = 2
Where Worker_Id between 4 and 7;

Update structure 
Set SupVis_Id = 3
Where Worker_Id between 8 and 10;


